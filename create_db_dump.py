import os

from datetime import datetime
from dotenv import load_dotenv


load_dotenv()


def db_data():
    db_host = os.getenv("DB_HOST", None)
    if not db_host:
        db_host = input(" > Enter db_host: ")
    db_name = os.getenv("DB_NAME", None)
    if not db_name:
        db_name = input(" > Enter db_name: ")
    db_user = os.getenv("DB_USER", None)
    if not db_user:
        db_user = input(" > Enter db_user: ")
    db_password = os.getenv("DB_PASS", None)
    if not db_password:
        db_password = input(" > Enter db_password: ")
    db_tables = os.getenv("DB_TABLES", None)
    if not db_tables:
        db_tables = input(" > Enter tables (separated by ,): ")

    db_table_param = [f"-t {tn}" for tn in db_tables.split(",")]
    return db_host, db_name, db_user, db_password, " ".join(db_table_param)


if __name__ == "__main__":
    host, name, user, passwd, tables = db_data()

    now = datetime.now().isoformat()
    dbname = f"postgresql://{user}:{passwd}@{host}:5432/{name}"
    os.system(f"mkdir -p {name}")
    os.system(f"pg_dump --dbname={dbname} {tables} > {name}/{name}_{now}.sql")
