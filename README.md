# Dump postgres database specifying tables

Database connection data and selected tables can be selected using .env or manually.

.env file
```
DB_HOST=
DB_NAME=
DB_USER=
DB_PASS=
DB_TABLES=
```

Dump file will be created under directory named like `DB_NAME`
